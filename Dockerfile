FROM mariadb:10.0 as base

LABEL maintainer="schneider@hrz.uni-marburg.de"

FROM base as prod
# customize:
# COPY PROD_STUFF /PROD_STUFF
ENV UMR_DIST_TARGET="prod"

FROM base as dev
# customize:
# COPY DEV_STUFF /DEV_STUFF
ENV UMR_DIST_TARGET="dev"
